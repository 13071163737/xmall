var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Goods = require('../models/goods');
var User = require("./../models/user");

mongoose.connect('mongodb://127.0.0.1:27017/xmall');

mongoose.connection.on("connected", function(){
    console.log("mongoDB connected success");
});

mongoose.connection.on("error", function(){
    console.log("mongoDB connected fail");
});

mongoose.connection.on("disconnected", function(){
    console.log("mongoDB connected disconnected");
});

/**
 * 获取商品列表数据
 */
router.get("/", function(req, res, next){
    let page = parseInt(req.param("page"));
    let pageSize = parseInt(req.param("pageSize"));
    let sort = req.param("sort");
    let priceLevel = req.param("priceLevel");
    let skip = (page - 1) * pageSize;
    let priceGte = 0, priceLt = 0;
    let params = {};
    if(priceLevel != "all"){
        switch(priceLevel){
            case "0" : priceGte = 0; priceLt = 500;break;
            case "1" : priceGte = 500; priceLt = 1000;break;
            case "2" : priceGte = 1000; priceLt = 2000;break;
            case "3" : priceGte = 2000; priceLt = 5000;break;
        }
        params = {
            salePrice: {
                $gte: priceGte,
                $lt: priceLt
            }
        }
    }
    let goodsModel = Goods.find(params).skip(skip).limit(pageSize);
    goodsModel.sort({"salePrice":sort});
    goodsModel.exec(function(err, doc){
        if(err){
            res.json({
                status: "1",
                message: err.message
            });
        }else{
            res.json({
                status: "0",
                msg: "",
                result: {
                    count: doc.length,
                    list: doc
                }
            });
        }
    });
});

/**
 * 加入到购物车
 */
router.post("/addCart", function(req, res, next){
    let userId = "100000077", productId = req.body.productId;
    User.findOne({userId: userId}, function(err, userList){
        if(err){
            res.json({
                status: "1",
                msg: err.message
            });
        }else{
            if(userList){
                let goodsItem = "";
                userList.cartList.forEach(function(item){
                    if(item.productId == productId){
                        item.productNum ++;
                        goodsItem = item;
                    }
                });
                if(goodsItem){
                    userList.save(function(err2, doc2){
                        if(err2){
                            res.json({
                                status: "1",
                                msg: err2.message
                            });
                        }else{
                            res.json({
                                status: "0",
                                msg: "success",
                                result: "addCart success"
                            });
                        }
                    });
                }else{
                    Goods.findOne({productId:productId}, function(err3, doc3){
                        if(err3){
                            res.json({
                                status: "1",
                                msg: err3.message
                            });
                        }else{
                            if(doc3){
                                doc3.productNum = 1;
                                doc3.checked = 1;
                                userList.cartList.push(doc3);
                                userList.save(function(err4, doc4){
                                    if(err4){
                                        res.json({
                                            status: "1",
                                            msg: err4.message
                                        });
                                    }else{
                                        if(doc4){
                                            res.json({
                                                status: "0",
                                                msg: "success",
                                                result: "addCart success"
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        }
    });
});

module.exports = router;