var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var produceSchema = new Schema({
    "productId": String,
    "productName": String,
    "prodcutPrice": Number,
    "prodcutImg": String,
    "productNum": Number,
    "checked": String
});

module.exports = mongoose.model("Good", produceSchema);